// ES6
import { Gitlab, Types } from '@gitbeaker/node'; // All Resources
//import { Types } from '@gitbeaker/node';
import { config } from 'dotenv'
//require('dotenv').config()
config()


const api = new Gitlab({
  token: process.env["GITLAB_TOKEN_ADMIN"],
  host: process.env["GITLAB_HOST"],
})


// let projects = await api.Projects.all({ maxPages: 2, perPage: 40 });


/*
Inside a main group (for example a corporation)
Create a sub-group that represents a "group project"
Then we'll create a "pm-management" project (repository)
*/
// create the group project
api.Groups.create("death-star-000", "death-star-000", {
  description:"this is my project",
  visibility:"public",
  parent_id: process.env["PARENT_GROUP_ID"]
}).then(group => {
  console.log("🙂[group]", group)

  // Create management project
  let groupId = group.id
  api.Projects.create({
    name: "management",
    visibility: "public",
    initialize_with_readme: true,
    namespace_id: groupId
  })
  .then(project => {
    console.log("🙂[project]", project)
  })
  .catch(error => {
    console.log("😡[project]", error)
  })

  // Create Epics and SubEpics
  api.Epics.create(groupId, "Project development")
  .then(epic => {
    console.log("🙂[epic]", epic)
    // create sub epics
    let parentId = epic.parent_id

    api.Epics.create(groupId, "Inception",{
      parent_id: parentId
    })


  })
  .catch(error => {
    console.log("😡[epic]", error)
  })

}).catch(error => {
  console.log("😡[group]", error)
})


